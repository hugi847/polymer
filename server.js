var express = require('express'),
  //  bodyParser = require('body-parser'),
  path = require('path'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/build/default'));
//app.use(bodyParser.json()); // for parsing application/json
//app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.listen(port);

console.log('Esperando que funciona Polymer desde node, puerto: ' + port);

// app.get('/', (req, res) => res.send('Hola mundo Node JS!!!'));
app.get('/', (req, res) =>
  res.sendFile('index.html', {
    root: '.'
  })
);
